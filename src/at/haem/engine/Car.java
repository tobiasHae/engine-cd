package at.haem.engine;

public class Car {

	private Engine engine;

	public Car(Engine engine) {
		super();
		this.engine = engine;
	}

	public Engine getEngine() {
		return engine;
	}

	
	public void addEngine(Engine engine) {
		this.engine = engine;
	}
	
	public void gas(int amount) {
		this.engine.run(amount /2);
	}
	
}
