package at.haem.engine;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Car c1 = new Car(new SuperFastGas(12345));
		
		c1.gas(6);
		
		c1.addEngine(new TopDiesel(456));
		
		c1.gas(12);
	}

}
