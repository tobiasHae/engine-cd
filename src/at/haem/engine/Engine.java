package at.haem.engine;

public interface Engine {

	public void run(int amount);
	public int getSerial();
}
