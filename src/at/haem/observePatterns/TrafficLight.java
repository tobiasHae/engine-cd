package at.haem.observePatterns;

public class TrafficLight implements Component {

	@Override
	public void start() {
		
		System.out.println("I have started!");
	}

	@Override
	public void isAlive() {
		
		System.out.println("I have started!");
	}

}
