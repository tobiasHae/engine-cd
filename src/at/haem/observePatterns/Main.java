package at.haem.observePatterns;

public class Main {

	public static void main(String[] args) {
		Component traf1 = new TrafficLight();
		Lantern lant = new Lantern();
		Observable chris = new Christmas();
		
		Sensor s1 = new Sensor();
		ManagementSystem m1 = new ManagementSystem();
		
		
		s1.addItem(chris);
		m1.addComponent(traf1);
		
		
		s1.addItem(lant);
		m1.addComponent(lant);
		
		m1.showComponents();
		s1.informAll();
	}

}
