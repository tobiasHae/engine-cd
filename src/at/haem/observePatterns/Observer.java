package at.haem.observePatterns;

public interface Observer {
	public void addItem(Observable obs);
	public void informAll();
}
