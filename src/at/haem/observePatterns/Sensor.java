package at.haem.observePatterns;

import java.util.ArrayList;
import java.util.List;

public class Sensor implements Observer {
	private List<Observable> observableList;
	

	public Sensor() {
		super();
		this.observableList = new ArrayList<Observable>();
	}

	@Override
	public void addItem(Observable obs) {
		this.observableList.add(obs);

	}

	@Override
	public void informAll() {
		for(Observable obs: this.observableList) {
			System.out.println("I am informing: " + obs);
		}
		

	}
	

}
