package at.haem.observePatterns;

public interface Observable {

	public void inform();
}
