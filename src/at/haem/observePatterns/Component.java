package at.haem.observePatterns;

public interface Component {
	public void start();
	public void isAlive();
}
