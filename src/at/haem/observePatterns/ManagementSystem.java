package at.haem.observePatterns;

import java.util.ArrayList;
import java.util.List;

public class ManagementSystem {
	private List<Component> componentList;

	public ManagementSystem() {
		super();
		this.componentList = new ArrayList<Component>();
	}
	
	public void addComponent(Component comp) {
		this.componentList.add(comp);
	}
	
	public void showComponents() {
		for(Component comp : this.componentList) {
			System.out.println("Another component: " + comp);
		}
	}
	
}
