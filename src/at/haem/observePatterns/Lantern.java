package at.haem.observePatterns;



public class Lantern implements Component,Observable {

	@Override
	public void inform() {
		System.out.println("I am informing everyone");
		
	}

	@Override
	public void start() {
		System.out.println("What's up?");
		
	}

	@Override
	public void isAlive() {
		System.out.println("What's up?");
		
	}

	
}
