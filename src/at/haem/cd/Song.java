package at.haem.cd;

public class Song implements Playable {
	private String name;
	private int id;
	
	
	
	
	public Song(String name, int id) {
		super();
		this.name = name;
		this.id = id;
	}


	
	
	

	public int getId() {
		return id;
	}






	public void setId(int id) {
		this.id = id;
	}






	@Override
	public void play() {
		System.out.println("This Song plays and it's name is: " + this.name);

	}

}
