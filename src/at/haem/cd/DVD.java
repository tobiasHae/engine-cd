package at.haem.cd;

import java.util.ArrayList;
import java.util.List;

public class DVD {
	private String title;
	private List<Title> titles;
	
	public DVD(String title) {
		super();
		this.title = title;
		this.titles = new ArrayList<Title>();
	}
	
	public void addTitles(Title title) {
		this.titles.add(title);
	}
	
	public void showTitles() {
		for(Title title : titles) {
			title.play();
		}
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	
	
	
	
	
}
