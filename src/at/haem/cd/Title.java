package at.haem.cd;

public class Title implements Playable {

	private String name;
	
	
	
	public Title(String name) {
		super();
		this.name = name;
	}
	
	


	@Override
	public void play() {
		System.out.println("This Title plays and it's name is: " + this.name);

	}

}
