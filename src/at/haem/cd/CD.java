package at.haem.cd;

import java.util.ArrayList;
import java.util.List;

public class CD {
	private String title;
	private List<Song> songs;
	
	
	public CD(String title) {
		super();
		this.title = title;
		this.songs = new ArrayList<Song>();
		
	}
	
	
	public void addSong(Song song) {
		this.songs.add(song);
	}

	
	
	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public void showTitles() {
		for(Song song: songs) {
			song.play();
		}
	}
	
	
	
}
