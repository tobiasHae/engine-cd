package at.haem.cd;

import java.util.ArrayList;
import java.util.List;

public class Player {

	private List<Playable> songsAndTitles;
	
	public Player() {
		super();
		this.songsAndTitles = new ArrayList<Playable>();
	}

	public void addSongOrTitle(Playable songOrTitle) {
		this.songsAndTitles.add(songOrTitle);
	}


	public void PlayAll(){
		for(Playable songOrTitle : songsAndTitles) {
			songOrTitle.play();
		}
	}
}
