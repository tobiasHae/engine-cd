package at.haem.factoryPattern;

import java.util.ArrayList;
import java.util.List;

public class Game {
	private List<Actor> actorList;

	public Game() {
		super();
		this.actorList = new ArrayList<Actor>();
	}
	
	public void addActor(Actor act) {
		this.actorList.add(act);
	}
	
	public void showActors() {
		for(Actor act : actorList) {
			System.out.println(act);
		}
	}
	public void renderActors() {
		for(Actor act : actorList) {
			act.render();
		}
	}
	public void moveActors() {
		for(Actor act : actorList) {
			act.move();
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Game g1 = new Game();
		for(int i = 0; i<4; i++) {
			g1.addActor(RandomActorFactory.createRandomActor());
		}
		
		
		g1.showActors();
		g1.moveActors();
		g1.renderActors();
	}
}
