package at.haem.factoryPattern;

public class RandomActorFactory {
		
	
		public static Actor createRandomActor() {
			double x = Math.random();
			if(x<0.5) {
				return new Snowflake();
			}
			else if(x>=0.5){
				return new Snowman();
				
			}
			return new Snowman();
		}
}
