package at.haem.factoryPattern;

public interface Actor {
	public void move();
	public void render();
}
