package at.haem.factoryPattern;

public class Snowflake implements Actor {

	@Override
	public void move() {
		System.out.println("I am a snowflake and I am moving");
		
	}

	@Override
	public void render() {
		System.out.println("I am a snowflake and I am rendering");
		
	}

}
