package at.haem.factoryPattern;

public class Snowman implements Actor {

	@Override
	public void move() {
		System.out.println("I am a snowman and I am moving");
		
	}

	@Override
	public void render() {
		System.out.println("I am a snowman and I am rendering");
		
	}

}
